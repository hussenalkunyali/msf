﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Xceed.Words.NET;
using System.Data;
using Microsoft.Office.Interop.Word;
using System.Net;
using Xceed.Document.NET;
using System.Text.RegularExpressions;

public partial class comed : System.Web.UI.Page
{
    Formatting fon = new Formatting();
    String connection = "Data Source=IT101\\SQL2014;Initial Catalog=barid_db;Integrated Security=True";
    SqlCommand cmd = new SqlCommand();
    SqlConnection conn = new SqlConnection();

    int boxnumber = 0;
    string pathwordbackupc = ""; 
    string deprtmamt = "";
    //string tables = "";
    string numbert = "";
    //string text = "";
    //string date = "";
    string to = "";
    //string username = "";
    //string sip = "";
    //string dep = "";
    //int numbu = 0;
    //string depnum = "";
    string pdfpath = "";
    int userid = 0;
    int nub = 0;
    int depid = 0;
    //int load1 = 0;
    int tst = 0;
    int Nouer = 0;
    string[] tests;
    List<string> idped = new List<string>();

    public Microsoft.Office.Interop.Word.Document wordDocument { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        conn.ConnectionString = connection;
        cmd.Connection = conn;
        conn.Close();
        if (Session["username"] != null)
        {
            if (Session["isAdmin"].ToString() != "4")
                Response.Redirect("box.aspx");
            if (Session["idbox"] == null)
                Response.Redirect("sender.aspx");
        }

    }

    protected void Findbox()
    {
        boxnumber = Convert.ToInt16(Session["idbox"].ToString());

        string comend = "select * from box_t where id_b =" + boxnumber + " ;";
        conn.Open();
        cmd.CommandText = comend;
        SqlDataReader read = cmd.ExecuteReader();
        while (read.Read())
        {
            tst += 1;
            numbert = read["number_b"].ToString();
            depid = Convert.ToInt32(read["to_u_b"].ToString());
            pathwordbackupc = read["pathwordbackup"].ToString();  
        }
        conn.Close();
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        if (Samenumber(TextBox1.Text))
            Label5.Text = "رقم الاشاري متكرر " + TextBox1.Text + " غير الرقم قبل الكتابة";
        else
            Label5.Text = "";
        Loadfs();
    }

    protected string Textpath (string txtpth)
    { txtpth = txtpth.Replace("\\","\\\\");
        return txtpth;  
    }

    public string Texted(string txed)
    {
        txed = txed.Replace("/", "-");
        txed = txed.Replace("\\", "-");
        txed = txed.Replace("*", "-");
        txed = txed.Replace("|", "-");
        txed = txed.Replace(":", "-");
        txed = txed.Replace("<", "-");
        txed = txed.Replace(">", "-");
        txed = txed.Replace("?", "-");
        txed = txed.Replace("؟", "-");
        txed = txed.Replace("\"", "-");
        return txed;
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Findbox();
        TextBox1.Text = "T-" + numbert;


        if (TextBox1.Text != "")
        {
            Loadfs();
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        SaveInWordCommnd();
        Dosend();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
       
    }

    protected void Loadfs()
    {

        conn.Close();
        conn.Open();
        string comdsql = "select  id_d,name_d from dep_t , user_t where id_d !='" + Session["dep00"].ToString() + "' AND user_name_u ='" + Session["username"] + "' AND name_d != 'صادر'";
        SqlDataAdapter sda = new SqlDataAdapter(comdsql, conn);
        System.Data.DataTable dt = new System.Data.DataTable();
        sda.Fill(dt);

        DropDownList2.DataSource = dt;
        DropDownList2.DataTextField = "name_d";
        DropDownList2.DataValueField = "id_d";
        DropDownList2.DataBind();
        conn.Close();

        conn.Open();
        comdsql = "select  id_d,name_d from dep_t , user_t where id_d !='" + Session["dep00"].ToString() + "' AND user_name_u ='" + Session["username"] + "' AND name_d != 'صادر' AND ID_D != 0";
        sda = new SqlDataAdapter(comdsql, conn);
        dt = new System.Data.DataTable();
        sda.Fill(dt);

        CheckBoxList1.DataSource = dt;
        CheckBoxList1.DataTextField = "name_d";
        CheckBoxList1.DataValueField = "id_d";
        CheckBoxList1.DataBind();
        conn.Close();
    }

    protected void Seldc()
    {
        

        string comend = "select * from copy_t where id_b =" + boxnumber + " ;";
        conn.Open();
        cmd.CommandText = comend;


        SqlDataReader read = cmd.ExecuteReader();

        while (read.Read())
        {
            idped.Add(read["id_u_c"].ToString());
        }
        conn.Close();

        //id_u_c

    }

    protected bool Samenumber(string numberenter)
    {
        conn.Close();
        string comend = "select count(*) from box_t where number_b ='" + Texted(numberenter) + "';";
        conn.Open();
        cmd.CommandText = comend;


        SqlDataAdapter sqa = new SqlDataAdapter(cmd);
        System.Data.DataTable tb = new System.Data.DataTable();

        sqa.Fill(tb);

        cmd.ExecuteNonQuery();
        if (tb.Rows[0][0].ToString() == "1")
        {
            conn.Close();
            return true;
        }

        else
        {
            conn.Close();
            return false;
        }
    }


    protected void SaveInWordCommnd()
    {

        Nouer = 1;
        fon.FontColor= System.Drawing.Color.DarkBlue;
        fon.Bold = true;
        Findbox();
        TextBox1.Text = Texted(TextBox1.Text);
        string newdate = Convert.ToString(DateTime.Today.Date.Year +
            "-" + DateTime.Today.Date.Month + "-" + DateTime.Today.Date.Day);
        var doc = DocX.Load(pathwordbackupc);
    
        doc.ReplaceText("##com"+Nouer+ "##",Request.Form["TextArea1"]+"\t"+ newdate, false , RegexOptions.IgnoreCase , fon );


        if (DropDownList2.Enabled == true && DropDownList2.Text != "")
            deprtmamt = DropDownList2.SelectedItem.Text;
        else
            deprtmamt = "صادر";

        // string paths ="E:\\barid\\blackbox\\test";


        pathwordbackupc = "E:\\barid\\blackbox\\send\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx";
        Label5.Text = pathwordbackupc;

        doc.SaveAs(pathwordbackupc);
        //  doc.SaveAs("E:\\barid\\blackbox\\send\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");

        Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();

        wordDocument = appWord.Documents.Open("E:\\barid\\blackbox\\send\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");
        pdfpath = "E:\\barid\\box\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf";
        wordDocument.ExportAsFixedFormat("E:\\barid\\box\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
        wordDocument.ExportAsFixedFormat("E:\\barid\\blackbox\\send\\pdf\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
        wordDocument.Close();
        appWord.Quit();


    }








    protected void Button4_Click(object sender, EventArgs e)
    {


        tst = 0;
        boxnumber = Convert.ToInt16(Session["idbox"].ToString());
        string comend = "select * from copy_t where id_b_c =" + boxnumber + " ;";
        conn.Open();
        cmd.CommandText = comend;
        SqlDataReader read = cmd.ExecuteReader();
        while (read.Read())
        {
            ++tst;
        }
        conn.Close();

        Label6.Text = tst.ToString();
        tests = new string[tst + 1];

        comend = "select * from copy_t where id_b_c =" + boxnumber + " ;";
        conn.Open();
        cmd.CommandText = comend;
        SqlDataReader read0 = cmd.ExecuteReader();
        tst = 0;
        while (read0.Read())
        {
            tests[tst] = read0["id_u_c"].ToString();
            tst += 1;
        }
        conn.Close();
        Label5.Text = tests[0];

        //List<ListItem> selected = new List<ListItem>();
        //foreach (ListItem item in CheckBoxList1.Items)
        //    if (item.Selected) Intable(Convert.ToInt16(item.Value));
        tst = 0;
        List<ListItem> selected = new List<ListItem>();
        foreach (ListItem item in CheckBoxList1.Items)
        {
            if (item.Value == tests[3])
                CheckBoxList1.SelectedItem.Selected = item.Selected;

            tst += 1;
        }
        Label6.Text = tst.ToString();
    }







    protected void Dosend()
    {
        userid = Convert.ToInt32(Session["uesernumber"].ToString());

        conn.Open();
        if (DropDownList2.Enabled == true)
            to = DropDownList2.SelectedValue;
        else
            to = "99"; //رقم صادر

        string comend = "INSERT INTO box_t (number_b , path_pdf_b, from_u_b ,to_u_b, date_b , pathwordbackup ) ";
        string valuss = "VALUES('" + TextBox1.Text + "','" + pdfpath + "','" + userid + "','" 
            + Convert.ToInt32(to) + "',GETDATE() , '" + pathwordbackupc + "' );";

        cmd.CommandText = comend + valuss;
        cmd.ExecuteNonQuery();
        conn.Close();

        List<ListItem> selected = new List<ListItem>();
        foreach (ListItem item in CheckBoxList1.Items)
            if (item.Selected) Intable(Convert.ToInt16(item.Value));

    }

    protected void Intable(int kk)
    {
        conn.Open();
        string comend = "INSERT INTO copy_t (id_b_c , id_u_c) VALUES(" + nub + "," + kk + ");";
        cmd.CommandText = comend;
        cmd.ExecuteNonQuery();
        conn.Close();
    }


    
}

/*
 
        List<ListItem> selected = new List<ListItem>();
        foreach (ListItem item in CheckBoxList1.Items)
            if (item.Selected) Intable(Convert.ToInt16(item.Value));

     */

///doc.ReplaceText("##com##", Request.Form["TextArea1"], false);
///
/// 
///
/// 
/// 
///
///number_b
///
/// 
/// 
/// 
/// 
/// 
///
/// 
/// 
/// 
/// 
/// 
///
/* protected void Saveinword()
    {

       TextBox1.Text= Texted(TextBox1.Text);

        //code save in word ;

        string newdate = Convert.ToString(DateTime.Today.Date.Year + "-" + DateTime.Today.Date.Month + "-" + DateTime.Today.Date.Day);

        var doc = DocX.Load("E:\\barid\\blackbox\\word-tm\\tm.docx");
        var docw = DocX.Load("E:\\barid\\blackbox\\word-tm\\wtm.docx");
        var docximage = doc.AddImage(sip);
        doc.ReplaceText("##Date_Date##", newdate, false);
        doc.ReplaceText("##number##", TextBox1.Text, false);

        if (DropDownList2.Enabled == true && DropDownList2.SelectedItem.Text != "")
            to = DropDownList2.SelectedItem.Text;
        else
            to = Texted(TextBox2.Text);

        doc.ReplaceText("##name_of_resever##", to);
        doc.ReplaceText("##text##", Request.Form["TextArea1"], false);

        doc.ReplaceText("##User_name##", username, false);

        doc.ReplaceText("##dep_name##", dep, false);
        doc.ReplaceText("##table##", Tablesdep(), false);

        var paragraphs = doc.Paragraphs.Where(x => x.Text.Contains("##image##"));
        foreach (var paragraph in paragraphs)
        {
            paragraph.InsertPicture(docximage.CreatePicture(100, 100), 0);

            paragraph.ReplaceText("##image##", "");
        }

        docw.ReplaceText("##Date_Date##", newdate, false);
        docw.ReplaceText("##number##", TextBox1.Text, false);


        /*
                if (DropDownList1.Text != "")
                    to = DropDownList1.Text;
                else
                    to = Texted(TextBox2.Text);*//*
docw.ReplaceText("##name_of_resever##", to);
        docw.ReplaceText("##text##", Request.Form["TextArea1"], false);

        docw.ReplaceText("##User_name##", username, false);

        docw.ReplaceText("##dep_name##", dep, false);
        docw.ReplaceText("##table##", Tablesdep(), false);
        docw.ReplaceText("##image##", "", false);


        if (numbu == 1)
        {

            doc.SaveAs("E:\\barid\\blackbox\\temp\\word\\" + dep + "\\" + TextBox1.Text + ".docx");
            Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();
wordDocument = appWord.Documents.Open("E:\\barid\\blackbox\\temp\\word\\" + dep + "\\" + TextBox1.Text + ".docx");
            wordDocument.ExportAsFixedFormat("E:\\barid\\blackbox\\temp\\pdf\\" + dep + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
            wordDocument.Close();


            /*****************************//*

            Response.Clear();
            string filePath = "E:\\barid\\blackbox\\temp\\pdf\\" + dep + "\\" + TextBox1.Text + ".pdf";
Response.ContentType = "application/pdf";
            Response.WriteFile(filePath);
            Response.End();

            /*
        
                Session["pathpdf"] = "E:\\barid\\blackbox\\temp\\pdf\\" + dep + "\\" + TextBox1.Text + ".pdf";

                string pageurl = "pdftap.aspx";
                Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");
   
            /*
             
        Response.Clear();
        string filePath = Session["pathpdf"].ToString() ;
        Response.ContentType = "application/pdf";
        Response.WriteFile(filePath);
        Response.End();*//*

        }

        if (numbu == 2)
        {


            if (DropDownList2.Enabled == true && DropDownList2.Text != "")
                deprtmamt =  DropDownList2.SelectedItem.Text;
            else
                deprtmamt = "صادر";
           
    
            Label6.Text = deprtmamt;


            //copy


            pathwordbackupc = "E:\\barid\\blackbox\\send\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx";
            Label5.Text = pathwordbackupc;

            doc.SaveAs(pathwordbackupc);

            docw.SaveAs("E:\\barid\\blackbox\\send\\withoutbackgrund\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");


            //  doc.SaveAs("E:\\barid\\blackbox\\send\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");

             Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();

wordDocument = appWord.Documents.Open("E:\\barid\\blackbox\\send\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");
             pdfpath = "E:\\barid\\box\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf";
             wordDocument.ExportAsFixedFormat("E:\\barid\\box\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
             wordDocument.ExportAsFixedFormat("E:\\barid\\blackbox\\send\\pdf\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
             wordDocument.Close();

            ////////////////////// *//*save in word with out background *//*
             wordDocument= appWord.Documents.Open("E:\\barid\\blackbox\\send\\withoutbackgrund\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");
             wordDocument.ExportAsFixedFormat("E:\\barid\\box\\wobg\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
             wordDocument.ExportAsFixedFormat("E:\\barid\\blackbox\\send\\withoutbackgrund\\pdf\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
             wordDocument.Close();
                                               
            }

    }*/