﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Xceed.Words.NET;
using System.Data;
using Microsoft.Office.Interop.Word;
using System.Net;


public partial class Nmessage : System.Web.UI.Page
{
    public Microsoft.Office.Interop.Word.Document wordDocument { get; private set; }

    String connection = "Data Source=IT101\\SQL2014;Initial Catalog=barid_db;Integrated Security=True";
    SqlCommand cmd = new SqlCommand();
    SqlConnection conn = new SqlConnection();
    string pathwordbackupc = "";
    string deprtmamt ="";
    string tables = "";
    string numbert = "";
    string text = "";
    string date = "";
    string to = "";
    string username = "";
    string sip="";
    string dep ="";
    int numbu = 0;
    string depnum = "";
    string pdfpath = "";
    int userid =0;
    int nub = 0;

    int load1 = 0;

 
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["p9p"] = "صادر";

        conn.ConnectionString = connection;
        date = Convert.ToString(DateTime.Today.Date.Year + "-" + DateTime.Today.Date.Month + "-" + DateTime.Today.Date.Day);
        Label4.Text = date + "\t" + DateTime.Today.Date.DayOfWeek;
    
        cmd.Connection = conn;
        conn.Close();

        if (Session["username"] != null && load1 ==0)
        {     //Depmm();
            
            //Load1();
            //load1 = 1;
            //string comdsql = "select  id_d,name_d from dep_t , user_t where id_d !='" + Session["dep00"].ToString() + "' AND user_name_u ='" + Session["username"] + "' AND name_d != 'صادر'";
            //SqlDataAdapter sda = new SqlDataAdapter(comdsql, conn);
            //System.Data.DataTable dt = new System.Data.DataTable();
            //sda.Fill(dt);

            //DropDownList2.DataSource = dt;
            //DropDownList2.DataTextField = "name_d";
            //DropDownList2.DataValueField = "id_d";
            //DropDownList2.DataBind();

            //Session["index"] = DropDownList2.SelectedIndex;



            //Session["txt1"] = DropDownList2.SelectedItem.Text;
            //Session["txt2"] = DropDownList2.SelectedItem.Value;


            //  DropDownList2.SelectedIndex = Convert.ToInt16(Session["index"].ToString());
            /*
                        RadioButtonList1.DataSource = dt;
                        RadioButtonList1.DataTextField = "name_d";
                        RadioButtonList1.DataValueField = "id_d";
                        RadioButtonList1.DataBind();
                        /* في المستقبل
                        CheckBoxList1.DataSource = dt;
                        CheckBoxList1.DataTextField = "name_d";
                        CheckBoxList1.DataValueField = "id_d";

                        CheckBoxList1.DataBind();
                        */

            // Session["select"] = DropDownList2.SelectedValue;

        }

      
    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
       
        if (CheckBox1.Checked)
        {
            TextBox2.Enabled = true;
            DropDownList2.SelectedIndex = 0;
            DropDownList2.Enabled = false;
        }
        else
        {
            TextBox2.Enabled = false;
            DropDownList2.Enabled = true;
            TextBox2.Text = "";
        }


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        TextBox1.Text = Texted(TextBox1.Text);
        if (TextBox1.Text == "")
        {
            TextBox1.Focus();
            
        }
        else { 
        numbu = 1;
        Conntionsql();
        Conntionsqldep();
        Saveinword();
       
        }
        load1 = 0; 
    }

protected void Button2_Click(object sender, EventArgs e)
    {
        TextBox1.Text = Texted(TextBox1.Text);
        if (TextBox1.Text == "" )
        {
            TextBox1.Focus();
     
        }

        else
        {
          if (Samenumber(TextBox1.Text))
            {
                Label5.Text = "لا يمكن ارسال هذا المحتوى بسبب تكرار رقم الإشاري";
                
            }
            else
            {
                numbu = 2;
                Conntionsql();
                Conntionsqldep();
                Saveinword();
                SendTo();
            }
        }
        load1 = 0;

    }

    protected void Saveinword()
    {

       TextBox1.Text= Texted(TextBox1.Text);

        //code save in word ;

        string newdate = Convert.ToString(DateTime.Today.Date.Year + "-" + DateTime.Today.Date.Month + "-" + DateTime.Today.Date.Day);

        var doc = DocX.Load("E:\\barid\\blackbox\\word-tm\\tm.docx");
        var docw = DocX.Load("E:\\barid\\blackbox\\word-tm\\wtm.docx");
        var docximage = doc.AddImage(sip);
        doc.ReplaceText("##Date_Date##", newdate, false);
        doc.ReplaceText("##number##", TextBox1.Text, false);

        if (DropDownList2.Enabled == true && DropDownList2.SelectedItem.Text != "")
            to = DropDownList2.SelectedItem.Text;
        else
            to = Texted(TextBox2.Text);

        doc.ReplaceText("##name_of_resever##", to);
        doc.ReplaceText("##text##", Request.Form["TextArea1"], false);

        doc.ReplaceText("##User_name##", username, false);

        doc.ReplaceText("##dep_name##", dep, false);
        doc.ReplaceText("##table##", Tablesdep(), false);

        var paragraphs = doc.Paragraphs.Where(x => x.Text.Contains("##image##"));
        foreach (var paragraph in paragraphs)
        {
            paragraph.InsertPicture(docximage.CreatePicture(100, 100), 0);

            paragraph.ReplaceText("##image##", "");
        }

        docw.ReplaceText("##Date_Date##", newdate, false);
        docw.ReplaceText("##number##", TextBox1.Text, false);
        /*
                if (DropDownList1.Text != "")
                    to = DropDownList1.Text;
                else
                    to = Texted(TextBox2.Text);*/
        docw.ReplaceText("##name_of_resever##", to);
        docw.ReplaceText("##text##", Request.Form["TextArea1"], false);

        docw.ReplaceText("##User_name##", username, false);

        docw.ReplaceText("##dep_name##", dep, false);
        docw.ReplaceText("##table##", Tablesdep(), false);
        docw.ReplaceText("##image##", "", false);


        if (numbu == 1)
        {

            doc.SaveAs("E:\\barid\\blackbox\\temp\\word\\" + dep + "\\" + TextBox1.Text + ".docx");
            Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();
            wordDocument = appWord.Documents.Open("E:\\barid\\blackbox\\temp\\word\\" + dep + "\\" + TextBox1.Text + ".docx");
            wordDocument.ExportAsFixedFormat("E:\\barid\\blackbox\\temp\\pdf\\" + dep + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
            wordDocument.Close();
            appWord.Quit();


            /*****************************/

            Response.Clear();
            string filePath = "E:\\barid\\blackbox\\temp\\pdf\\" + dep + "\\" + TextBox1.Text + ".pdf";
            Response.ContentType = "application/pdf";
            Response.WriteFile(filePath);
            Response.End();

            /*
        
                Session["pathpdf"] = "E:\\barid\\blackbox\\temp\\pdf\\" + dep + "\\" + TextBox1.Text + ".pdf";

                string pageurl = "pdftap.aspx";
                Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");
   
            /*
             
        Response.Clear();
        string filePath = Session["pathpdf"].ToString() ;
        Response.ContentType = "application/pdf";
        Response.WriteFile(filePath);
        Response.End();*/

        }

        if (numbu == 2)
        {


            if (DropDownList2.Enabled == true && DropDownList2.Text != "")
                deprtmamt =  DropDownList2.SelectedItem.Text;
            else
                deprtmamt = "صادر";
           
    
            Label6.Text = deprtmamt;


            //copy


            pathwordbackupc = "E:\\barid\\blackbox\\send\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx";
            Label5.Text = pathwordbackupc;

            doc.SaveAs(pathwordbackupc);

            docw.SaveAs("E:\\barid\\blackbox\\send\\withoutbackgrund\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");


            //  doc.SaveAs("E:\\barid\\blackbox\\send\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");

             Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();

             wordDocument = appWord.Documents.Open("E:\\barid\\blackbox\\send\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");
             pdfpath = "E:\\barid\\box\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf";
             wordDocument.ExportAsFixedFormat("E:\\barid\\box\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
             wordDocument.ExportAsFixedFormat("E:\\barid\\blackbox\\send\\pdf\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
             wordDocument.Close();
            

            ////////////////////// *//*save in word with out background */
             wordDocument= appWord.Documents.Open("E:\\barid\\blackbox\\send\\withoutbackgrund\\word\\" + deprtmamt + "\\" + TextBox1.Text + ".docx");
             wordDocument.ExportAsFixedFormat("E:\\barid\\box\\wobg\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
             wordDocument.ExportAsFixedFormat("E:\\barid\\blackbox\\send\\withoutbackgrund\\pdf\\" + deprtmamt + "\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
             wordDocument.Close();

            appWord.Quit();
        }
  
    }
 public string Texted (string txed)
        {


        txed = txed.Replace("/", "-");
        txed = txed.Replace("\\", "-");
        txed = txed.Replace("*", "-");
        txed = txed.Replace("|", "-");
        txed = txed.Replace(":", "-");
        txed = txed.Replace("<", "-");
        txed = txed.Replace(">", "-");
        txed = txed.Replace("?", "-");
        txed = txed.Replace("؟", "-");
        txed = txed.Replace("\"", "-");


        return txed;
        } 

  

    protected void Conntionsql()
    {
        try { 
      
        string comend = "select * from user_t where user_name_u ='" + Session["username"] + "' and epassword_u ='" + Session["password"] + "';";
        conn.Open();
        cmd.CommandText = comend;
   

        SqlDataReader read = cmd.ExecuteReader();
       
            while (read.Read())
            {
                userid = Convert.ToInt16(read["enum_u"].ToString());
                username = read["ename_u"].ToString();
                sip = read["si_path_u"].ToString();
                Label5.Text = sip;
            }
            conn.Close();

           
        }
        catch (InvalidCastException e)
        { Label5.Text =Convert.ToString(e); }
    }

    protected void Conntionsqldep()
    {
        string comend = "select * from user_t , dep_t  where user_name_u ='" + Session["username"] + "' and epassword_u ='" + Session["password"] + "' and id_d = dep_u ;";
        conn.Open();
        cmd.CommandText = comend;


        SqlDataReader read = cmd.ExecuteReader();

        while (read.Read())
        {
            depnum = read["id_d"].ToString();
            dep =  read["name_d"].ToString();
            Label5.Text = dep;
        }
        conn.Close();

    }



    protected string Tablesdep()
    {

        List<ListItem> selected = new List<ListItem>();
        foreach (ListItem item in CheckBoxList1.Items)
            if (item.Selected) tables += item.Text + "\n";
        tables += TextBox3.Text;
        return tables;
    }



    protected bool Samenumber(string numberenter)
    {
        conn.Close();
        string comend = "select count(*) from box_t where number_b ='" + Texted(numberenter)+"';";
        conn.Open();
        cmd.CommandText = comend;


        SqlDataAdapter sqa = new SqlDataAdapter(cmd);
        System.Data.DataTable tb = new System.Data.DataTable();

        sqa.Fill(tb);

        cmd.ExecuteNonQuery();
        if (tb.Rows[0][0].ToString() == "1")
        {
            conn.Close();
            return true;
        }

        else
        {
            conn.Close();
            return false;
        }
    }


    // Response.Write("<script>alert('Data inserted successfully')</script>");


    protected void SendTo()
    {

    
        //"path" "Usename" "TO" "Nunber" "copys" 

        // INSERT INTO box_t (number_b , path_pdf_b, from_u_b ,to_u_b,date_b,opened_b)
        //VALUES(
        conn.Close();

        if (DropDownList2.SelectedItem.Text == "" && DropDownList2.Enabled == true)
        {
            Label5.Text = "اختار الجها المرسلة";
            return;
        }

        if (TextBox2.Enabled == true && TextBox2.Text == "")
        {
            Label5.Text = "اختار الجها المرسلة";
            return;
        }

        conn.Open();

        if (DropDownList2.Enabled==true)
            to = DropDownList2.SelectedValue;
        else
            to = "99"; //رقم صادر

        string comend = "INSERT INTO box_t (number_b , path_pdf_b, from_u_b ,to_u_b, date_b , pathwordbackup ) ";
        string valuss ="VALUES('"+TextBox1.Text+"','"+ pdfpath+"','"+ userid +"','"+ Convert.ToInt16(to)+ "',GETDATE() , '"+pathwordbackupc+"' );";
       
        cmd.CommandText = comend+ valuss;
        cmd.ExecuteNonQuery();
        conn.Close();

        //////////////////////////////////////////////////

        comend = "select id_b from box_t where  number_b='"+TextBox1.Text+"';";
        conn.Open();
        cmd.CommandText = comend;

        SqlDataReader read = cmd.ExecuteReader();

        while (read.Read())
        {
            nub = Convert.ToInt16(read["id_b"].ToString());
        }
        conn.Close();


        Label5.Text = "تم ارسال رسالة\t "+TextBox1.Text+"الي\t"+DropDownList2.SelectedItem.Text+"";

        /////////////////////////////////////////////


        List<ListItem> selected = new List<ListItem>();
        foreach (ListItem item in CheckBoxList1.Items)
            if (item.Selected) Intable(Convert.ToInt16(item.Value));

    }

    protected void Intable(int kk)
    {
        conn.Open();
        string comend = "INSERT INTO copy_t (id_b_c , id_u_c) VALUES(" + nub + ","+kk+");";
        cmd.CommandText = comend;
        cmd.ExecuteNonQuery();
        conn.Close();
    }

    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {
       TextBox3.Text=  TextBox2.Text; 
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        var doc = DocX.Load("E:\\barid\\tm.docx");
        doc.SaveAs("E:\\barid\\" + TextBox1.Text + ".docx");
        Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();
        wordDocument = appWord.Documents.Open("E:\\barid\\" + TextBox1.Text + ".docx");
        wordDocument.ExportAsFixedFormat("E:\\barid\\" + TextBox1.Text + ".pdf", WdExportFormat.wdExportFormatPDF);
        wordDocument.Close();
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        if (Samenumber(TextBox1.Text))
            Label5.Text = "رقم الاشاري متكرر " + TextBox1.Text + " غير الرقم قبل الكتابة";
        else
            Label5.Text = "";
       Load1();
    }

    protected void Load1()
    {
        conn.Close();
        conn.Open();
        string comdsql = "select  id_d,name_d from dep_t , user_t where id_d !='" + Session["dep00"].ToString() + "' AND user_name_u ='" + Session["username"] + "' AND name_d != 'صادر'";
        SqlDataAdapter sda = new SqlDataAdapter(comdsql, conn);
        System.Data.DataTable dt = new System.Data.DataTable();
        sda.Fill(dt);

        DropDownList2.DataSource = dt;
        DropDownList2.DataTextField = "name_d";
        DropDownList2.DataValueField = "id_d";
        DropDownList2.DataBind();
        conn.Close();

        conn.Open();
        comdsql = "select  id_d,name_d from dep_t , user_t where id_d !='" + Session["dep00"].ToString() + "' AND user_name_u ='" + Session["username"] + "' AND name_d != 'صادر' AND ID_D != 0";
        sda = new SqlDataAdapter(comdsql, conn);
        dt = new System.Data.DataTable();
        sda.Fill(dt);

        CheckBoxList1.DataSource = dt;
        CheckBoxList1.DataTextField = "name_d";
        CheckBoxList1.DataValueField = "id_d";
        CheckBoxList1.DataBind();
        conn.Close();
    }
}


/*

    var paragraphs = doc.Paragraphs.Where(x => x.Text.Contains("##image##"));
        foreach (var paragraph in paragraphs)
        {
            paragraph.InsertPicture(docximage.CreatePicture(100, 100), 0);
            paragraph.InsertPicture(docximage0.CreatePicture(100, 100), 0);
            paragraph.ReplaceText("##image##", "");
        }

*/
/*

comend = "select name_d from user_t and where user_name_u ='" + Session["username"] + "' and epassword_u ='" + Session["password"] + "' and id_d=dep_u;";
           conn.Open();
           cmd.CommandText = comend;

           SqlDataReader read0 = cmd.ExecuteReader();
           read0 = cmd.ExecuteReader();

           while (read0.Read())
           {
               dep = read0["name_d"].ToString();


               Label5.Text = dep;
           }
           conn.Close();
*/
/*

       if (numbu == 2)
       {
           if (DropDownList1.Enabled == true && DropDownList1.SelectedValue != "")
               deprtmamt = DropDownList1.SelectedValue;
           else
               deprtmamt = "صادر";
           doc.SaveAs("E:\\barid\\blackbox\\temp\\" + dep + "\\" + newdate + ".docx");



           doc.SaveAs("E:\\barid\\blackbox\\word\\" + dep + "\\" + TextBox1.Text + ".docx");

           Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();

           wordDocument = appWord.Documents.Open("E:\\barid\\blackbox\\temp\\" + deprtmamt + "\\" + newdate + ".docx");

           wordDocument.ExportAsFixedFormat("E:\\barid\\box\\" + deprtmamt + "\\" + newdate + ".pdf", WdExportFormat.wdExportFormatPDF);
           wordDocument.Close();


           // doc.SaveAs("E:\\barid\\blackbox\\temp\\" + dep + "\\" + newdate + ".docx");


           //  doc.SaveAs("E:\\barid\\box\\it\\" + DropDownList1.DataValueField + "\\" + TextBox1.Text + newdate + ".docx");

       }
*/

/*
 select id_d, name_d, ename_u 
from dep_t , user_t
where id_d != dep_u AND user_name_u ='HUSSEN'


 */
