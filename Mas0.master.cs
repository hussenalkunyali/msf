﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mas0 : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["username"] == null)
            Response.Redirect("login.aspx");
        else
            Label1.Text = Session["username"].ToString();

    }


    protected void Button1_Click1(object sender, EventArgs e)
    {
        Session["username"] = null;
        Response.Redirect("login.aspx");
    }
}
