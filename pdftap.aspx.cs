﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pdftap : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Response.Clear();
        string filePath = Session["pathpdf"].ToString() ;
        Response.ContentType = "application/pdf";
        Response.WriteFile(filePath);
        Response.End();

    }
}